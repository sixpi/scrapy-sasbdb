# -*- coding: utf-8 -*-
import re
import os

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

from sasbdb.items import SasbdbItem

FIELD_XPATHS = {
    'uniprot': u'//'
}


class SasbdbSpider(CrawlSpider):
    name = "sasbdb"
    # allowed_domains = ["www.sasbdb.org"]
    start_urls = (
        'http://www.sasbdb.org/browse/',
    )
    rules = (
        Rule(LinkExtractor(allow=(r'page=',), unique=True)),
        Rule(LinkExtractor(allow=(r'data',), unique=True), callback='parse_entry')
    )

    def parse_entry(self, response):
        sasbdb_id = response.url.strip("/").split("/")[-1]
        item = SasbdbItem(sasbdb_id=sasbdb_id)
        url = response.url

        item['url'] = url

        title = "".join(response.xpath(
            '//*[@id="body-padding"]/h1//text()').extract()).replace(u" \u2013", "-")
        item['title'] = title

        item['molecule_name'] = response.css(".molecule-name").extract()[0]

        return item
