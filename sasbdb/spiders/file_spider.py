# -*- coding: utf-8 -*-
import re
import os

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

from sasbdb.items import SasbdbItem

FIELD_XPATHS = {
    'uniprot': u'//'
}


class SasbdbFileSpider(CrawlSpider):
    name = "sasbdb_files"
    # allowed_domains = ["www.sasbdb.org"]
    start_urls = (
        'http://www.sasbdb.org/browse/',
    )
    rules = (
        Rule(LinkExtractor(allow=(r'page=',), unique=True)),
        Rule(LinkExtractor(allow=(r'data',), unique=True), callback='parse_entry')
    )

    def parse_entry(self, response):
        sasbdb_id = response.url.strip("/").split("/")[-1]
        item = SasbdbItem(sasbdb_id=sasbdb_id)
        item['url'] = response.url

        file_urls = response.xpath('//div[@id="download_menu_body"]//a/@href').extract()
        item['file_urls'] = [{
            "url": 'http://www.sasbdb.org' + url,
            "path": sasbdb_id + '/' +os.path.basename(url)
        } for url in file_urls]

        return item
