# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
from scrapy import Item, Field


class SasbdbItem(Item):
    sasbdb_id = Field()
    title = Field()
    url = Field()

    radius_of_gyration = Field()
    molecular_weight = Field()
    molecule_type = Field()
    oligomer_state = Field()
    uniprot_id = Field()
    sequence = Field()
    molecule_name = Field()

    file_urls = Field()
    files = Field()
