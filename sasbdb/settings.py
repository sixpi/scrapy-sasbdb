# -*- coding: utf-8 -*-

# Scrapy settings for sasbdb project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'sasbdb'

SPIDER_MODULES = ['sasbdb.spiders']
NEWSPIDER_MODULE = 'sasbdb.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'sasbdb (+http://www.yourdomain.com)'
ITEM_PIPELINES = [
    'sasbdb.files.FilesPipeline'
]
FILES_STORE = '/home/bing/data/sasbdb/storage'
